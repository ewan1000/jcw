---
header_image: "images/cover-image.jpeg"
header_headline: "Jane's Cleaning Services"
header_subheadline: "Hi there, I have been providing private & commercial cleaning services in the Auckland region for over 14 years"
---
