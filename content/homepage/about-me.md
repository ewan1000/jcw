---
title: "About Me"
weight: 3
header_menu: true
---

##### Professional care

I take immense pride in providing individualised care for my clients. As such, I take the time to get to know my clients and their needs.

Whether you need help for your business, yourself, or members of your family, we can talk about what works best for you. 

##### I offer

- Flexibility
- Transparency
- Individualised care

