#### Jane's Cleaning Website (JCW)

This repository has content filed away (unsurprisingly)  in the `content` folder.

You can make changes to the content and submit what's called a `pull request`, once I see your changes on my side, i'll accept and the website will rebuild itself.

If you want new pages/features/styles, get stuck, or break it let me know :)

O
